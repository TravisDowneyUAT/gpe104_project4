﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public AudioMixer audioMixer; //reference to an audio mixer
    public Dropdown resolutionDropdown; //reference to a dropdown menu
    Resolution[] resolutions; //array to hold screen resolutions

    void Start()
    {
        resolutions = Screen.resolutions; //set the resolutions array to the available resolutions for the given screen

        resolutionDropdown.ClearOptions(); //clear out the options that were initially put into the dropdown menu

        List<string> options = new List<string>(); //create a new List of strings to display the resolution options

        int currentResIndex = 0; //int to hold the current index for the resolution array

        //run as long as i is less than the length of the resolutions array
        for (int i = 0; i < resolutions.Length; i++)
        {
            /*string variable to hold display the resolution by getting the
             width and height of the resolutions in the resolutions array*/
            string option = resolutions[i].width + "x" + resolutions[i].height;

            //add the newly created option to the options list
            options.Add(option);

            //make sure the current resolution index is equal to whatever the i variable equals in the
            //current run of the for loop
            if (resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentResIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options); //add the objects from the options list to the options of the dropdown menu
        resolutionDropdown.value = currentResIndex; //make sure the value of the dropdown menu is equal to the current resolution index
        resolutionDropdown.RefreshShownValue(); //refresh the dropdown so the visual of the dropdown corresponds to the added options
    }

    public void SetVolume(float volume)
    {
        //set the Volume parameter of the audio mixer to the float chosen by the volume slider
        audioMixer.SetFloat("Volume", volume);   
    }

    public void SetFullscreen(bool isFullscreen)
    {
        //set the game to be fullscreen depending on if the fullscreen toggle is true or false
        Screen.fullScreen = isFullscreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        //set the screen resolution based on the whatever was chosen from the dropdown
        //and if the game is set to fullscreen or not
        Resolution resolution = resolutions[resolutionIndex];

        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }
}
