﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private GameManager manager; //reference to the GameManager

    public float moveSpeed; //public move speed variable
    public float jumpForce; //public jump force variale
    private float moveInput; //float to hold the move input from the player

    private Rigidbody2D rb; //reference to the player's rigid body component
    public SpriteRenderer sr; //reference to a sprite renderer component
    public Animator animator; //reference to an animator

    private bool isGrounded; //bool to detect if the player is grounded
    public Transform groundCheck; //transform to hold a ground check object
    public float checkRadius; 
    public LayerMask whatIsGround; //layer mask to tell the player what is ground

    private int extraJumps; //int to hold extra jumps for the player
    public int extraJumpValue; //int to modify how many extra jumps the player gets

    void Start()
    {
        extraJumps = extraJumpValue; //setting the amount of extra jumps to the amount defined in the inspector

        rb = GetComponent<Rigidbody2D>(); //setting the reference to the Rigidbody2D component on the player

        //setting the reference to the GameManager script
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        //if the player is moving to the right then have the player face right
        //else if the player is moving to the left have the player face left
        if (moveInput > 0)
        {
            sr.flipX = false;
        }
        else if (moveInput < 0)
        {
            sr.flipX = true;
        }

        //if the player is on the ground then set make sure the jump animation has ended
        //and reset the extra jumps to value defined in the inspector
        if(isGrounded == true)
        {
            animator.SetBool("isJumping", false);
            extraJumps = extraJumpValue;
        }

        /*if the player presses the jump button and has some extra jumps left over then
         propel the player up by how much jump force is defined in the inspector,
         play the jump animation and the PlayerJump sound clip,
         and decrease the amount of extra jumps by 1*/
        if (Input.GetButtonDown("Jump") && extraJumps > 0)
        {
            rb.velocity = Vector2.up * jumpForce;
            FindObjectOfType<AudioManager>().Play("PlayerJump");
            animator.SetBool("isJumping", true);
            extraJumps--;
        } 
        //else if the player presses the jump button, is out of extra jumps, and isGrounded is set to true then just propel the player up
        else if(Input.GetButtonDown("Jump") && extraJumps == 0 && isGrounded == true)
        {
            rb.velocity = Vector2.up * jumpForce;   
        }
    }

    void FixedUpdate()
    {
        /*detect is the player is grounded based on an overlap circle at the ground check's position,
         * inside the check radius defined in the inspector and on the layer chosen in the layer mask*/
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);

        //if the buttons on the Horizontal axis are pressed then play the PlayerFootsteps sound clip
        if (Input.GetButtonDown("Horizontal"))
        {
            FindObjectOfType<AudioManager>().Play("PlayerFootsteps");
        }

        moveInput = Input.GetAxisRaw("Horizontal"); //set the moveInput to the value from the Horizontal axis input
        animator.SetFloat("Speed", Mathf.Abs(moveInput)); //run the moving animation based on the absolute value of the moveInput variable
        //move the player based on the moveInput multiplied by the moveSpeed defined in the inspector
        //and the linear velocity of the rigidbody on the y-axis
        rb.velocity = new Vector2(moveInput * moveSpeed, rb.velocity.y);
    }
}
