﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirtyPointPickup : MonoBehaviour
{
    private GameManager manager; //reference to the GameManager
    private int score = 30; //int to hold the score of the prefab

    private void Start()
    {
        //setting the refernce to the GameManager script
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player passes through this game object then increase the player's score
        //and deactivate the game object
        if (collision.gameObject.CompareTag("Player"))
        {
            manager.IncreaseScore(score);
            gameObject.SetActive(false);

        }
    }
}
