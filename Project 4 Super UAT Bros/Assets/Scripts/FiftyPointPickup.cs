﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiftyPointPickup : MonoBehaviour
{
    private GameManager manager; //reference to the GameManager
    private int score = 50; //int to hold the score of the prefab

    private void Start()
    {
        //link the reference to the GameManager script
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player passes into the collider then deactivate the game object and increase the player's score
        //based on the provided score for this prefab
        if (collision.gameObject.CompareTag("Player"))
        {
            manager.IncreaseScore(score);
            gameObject.SetActive(false);

        }
    }
}
