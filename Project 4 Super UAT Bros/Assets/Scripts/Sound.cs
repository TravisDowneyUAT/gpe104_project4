﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound
{
    public string name; //string to hold the name of the audio clip

    public AudioClip clip; //reference to the audio clip
    public AudioMixerGroup group; //reference to the audio mixer

    public bool loop; //bool on if we want the sound to loop or not

    [Range(0f,1f)]
    public float volume; //make a volume slider with a range of 0 to 1
    [Range(0.1f,3f)]
    public float pitch; //make a pitch slider with a range of 0.1 to 3

    //make a new audio source
    [HideInInspector]
    public AudioSource source;
}
