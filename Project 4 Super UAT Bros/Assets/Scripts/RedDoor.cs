﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RedDoor : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player passes into the collider then play the PlayerWin sound clip
        //and load the Victory scene
        if (collision.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<AudioManager>().Play("PlayerWin");
            SceneManager.LoadScene("Victory");
        }
    }
}
