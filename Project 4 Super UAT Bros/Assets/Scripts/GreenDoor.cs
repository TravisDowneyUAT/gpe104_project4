﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GreenDoor : MonoBehaviour
{
    private GameManager manager; //reference to the GameManager

    private void Start()
    {
        //link the reference to the GameManager script
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player passes into the collider then play the PlayerWin sound clip
        //Load the next scene in the build index
        //and reset the last checkpoint position to the center of the screen
        if (collision.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<AudioManager>().Play("PlayerWin");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            manager.lastCheckPointPos = Vector3.zero;
        }
    }
}
