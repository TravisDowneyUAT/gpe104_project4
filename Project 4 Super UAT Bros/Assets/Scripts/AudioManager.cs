﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance; //make a static reference to the instance of the AudioManager

    public Sound[] sounds; //array to hold the sounds in the manager

    // Start is called before the first frame update
    void Awake()
    {
        //if there is no instance of the AudioManager in the scene make this game object that instance
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        
        //for each sound in the sounds array
        foreach (Sound s in sounds)
        {
            //add in a new audio source component
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip; //make the clip of the source equal to the clip in the sounds array

            s.source.volume = s.volume; //make the volume of the source equal to the volume in the sounds array
            s.source.pitch = s.pitch; //make the pitch of the source equal to the pitch in the sounds array
            s.source.loop = s.loop; //have the sound loop if the loop bool is toggled int the sounds array
            s.source.outputAudioMixerGroup = s.group; //make the audio group of the source equal to the audio group chosen in the sounds array
        }
    }

    private void Start()
    {
        Play("ThemeMusic"); //play the clip with the name ThemeMusic
    }

    public void Play(string name)
    {
        //find the sound based on the name in the sounds array and then play that sound from the audio source
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }
    public void Pause(string name)
    {
        //find the sound based on the name in the sounds array and then pause that sound from the audio source
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Pause();
    }
    public void Stop(string name)
    {
        //find the sound based on the name in the sound array and then stop playing the sound completely from the audio source
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Stop();
    }
}
