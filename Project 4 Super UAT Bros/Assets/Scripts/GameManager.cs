﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance; //static reference to the GameManager

    public GameObject playerPrefab; //reference to the player
    public GameObject thirtyPointPrefab; //reference to a thirty point gem
    public GameObject fiftyPointPrefab; //reference to a fifty point gem
 
    //Vector2 to hold the position of the last checkpoint the player passed
    [HideInInspector]
    public Vector2 lastCheckPointPos;

    private int playerLives; //int to hold the amount of lives the player has
    private int playerScore; //int to hold the player's score

    public Text playerLivesText; //reference to the text object that displays the player's lives
    public Text playerScoreText; //refernce to the text object that displays the player's score

    private void Awake()
    {
        //if there is no GameManager object then make the instance equal this game object and don't destroy it
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        GameStart();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GameStart()
    {
        playerLives = 3; //set the lives of the player to 3
        playerScore = 0; //set the player's score to 0

        playerLivesText.text = "Lives: " + playerLives; //display the player's current lives on screen
        playerScoreText.text = "Points: " + playerScore; //display the player's current score on screen

        SpawnPickups();
    }

    void SpawnPickups()
    {
        //spawn thirty and fifty point prefabs at certain points in the scene when the game starts
        Instantiate(thirtyPointPrefab, new Vector2(12f,-0.7f),Quaternion.identity);
        Instantiate(fiftyPointPrefab, new Vector2(80f, 9f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(83f, 9f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(113f, -9f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(145f, -12f), Quaternion.identity);
        Instantiate(fiftyPointPrefab, new Vector2(147f, -12f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(181f, -28f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(186f, -28f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(206f, -25f), Quaternion.identity);
        Instantiate(fiftyPointPrefab, new Vector2(226f, -18f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(243f, -14f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(232f, -9f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(228.5f, -9f), Quaternion.identity);
        Instantiate(thirtyPointPrefab, new Vector2(216f, -5f), Quaternion.identity);
        Instantiate(fiftyPointPrefab, new Vector2(262f, -25f), Quaternion.identity);
    }

    public void IncreaseScore(int newScore)
    {
        //increase the player's score based on however many points the item that the player picks up is worth
        playerScore += newScore; 

        //update the player score on screen
        playerScoreText.text = "Points: " + playerScore;
    }

    public void DecreaseLives()
    {
        playerLives--; //decrease the player's lives by 1

        //if the player's lives ever drop below 0 then load the GameOver screen
        if(playerLives <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
