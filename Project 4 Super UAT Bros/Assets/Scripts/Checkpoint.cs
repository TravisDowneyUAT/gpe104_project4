﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private GameManager manager; //reference to the GameManager
    public Animator animator; //reference to an animator
    private void Start()
    {
        //link the refernce to the GameManager script
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        //if the player passes the checkpoint then set the last checkpoint position in the GameManager
        //to this object position in the scene and set the isActive bool in the animator to true
        if (col.CompareTag("Player"))
        {
            manager.lastCheckPointPos = transform.position;
            animator.SetBool("isActive", true);
        }
    }
}
