﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPos : MonoBehaviour
{
    private GameManager manager; //reference to the GameManager
    public GameObject gDoor; //reference to the Green Door game object
    public GameObject rDoor; //reference to the Red Door game object

    // Start is called before the first frame update
    void Start()
    {
        //link the reference to the GameManager script
        //and set the player's position to the last checkpoint they passed
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        transform.position = manager.lastCheckPointPos;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player hits anything with a Spike or Liquid tag then kill the player
        //decrease their lives, play the PlayerDeath sound clip and reload the level
        if (collision.gameObject.CompareTag("Spike") || collision.gameObject.CompareTag("Liquid"))
        {
            Destroy(gameObject);
            manager.DecreaseLives();
            FindObjectOfType<AudioManager>().Play("PlayerDeath");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        //if the player hits anything with the GKey tag then deactivate that object and activate the Green Door
        if (collision.gameObject.CompareTag("GKey"))
        {
            collision.gameObject.SetActive(false);
            gDoor.SetActive(true);
        }

        //if the player hits anything with the RKey tag then deactivate that object and activate the Red Door
        if (collision.gameObject.CompareTag("RKey"))
        {
            collision.gameObject.SetActive(false);
            rDoor.SetActive(true);
        }

    }
}
