﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverVictoryMenu : MonoBehaviour
{
    public void RestartGame()
    {
        //if the player hits the restart button load the MainMenu scene
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
